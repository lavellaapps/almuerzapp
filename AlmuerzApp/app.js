var express = require('express');
var bodyParser = require('body-parser'); 
var app = express(); 
app.use(bodyParser.json()); 
app.use(express.static('static'));
//app.use('/static', express.static('static'));

app.use(function(req, res, next) {
	  res.header('Access-Control-Allow-Origin', "*");
	  res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');
	  res.header('Access-Control-Allow-Headers', 'Content-Type');
	  if (req.method == 'OPTIONS') {
	    res.status(200).send();
	} else {
		next(); 
	}
});


app.get('/hello', function(req, res) { 
	res.end('Hola Mundo!!');
});

//SQLite

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('mydb.db');
var check;
db.serialize(function() {
	/*
	db.run("DROP TABLE bares", [], function(err) { 
		if (err) { console.log('Error: ' + err); }
		else  { console.log("Borrada la base de datos"); }	
	});
	*/
	
	db.run("CREATE TABLE if not exists bares (id INTEGER PRIMARY KEY, titulo TEXT, visible TEXT, descripcion TEXT, fecha DATE, posiciones TEXT, fotos TEXT, direccion TEXT, latitud DOUBLE, longitud DOUBLE)");
  
  
  
   
  
  /*
  db.run("DELETE FROM bares", [], function(err) { 
		if (err) { console.log('Error: ' + err); }
		else  { console.log("Borrada la base de datos"); }	
	});
  /*
  var stmt = db.prepare("INSERT INTO bares VALUES (?,'on','#000000','','')");
  for (var i = 0; i < 10; i++) {
      stmt.run("Hola " + i);
  }
  stmt.finalize();
   
  db.each("SELECT rowid AS id, titulo FROM bares", function(err, row) {
      console.log(row.id + ": " + row.titulo);
  });
  */
});

//db.close();

app.post('/misbares/bares', function(req, res) { 
	console.log('POST /misbares/bares');
	var bar = req.body;
	db.run("INSERT INTO bares (titulo, visible, descripcion, fecha, posiciones, fotos, direccion, latitud, longitud) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? )", [bar.titulo, bar.visible, bar.descripcion, bar.fecha, JSON.stringify(bar.posiciones), JSON.stringify(bar.fotos), bar.direccion, bar.latitud, bar.longitud], function(err) { 
		if (err) { console.log('Error: ' + err); }
		else {
			bar.id = this.lastID;
			res.send(bar);
		}
	});
    
});

app.get('/misbares/bares', function(req, res) { 
	console.log('GET /misbares/bares');
	db.all("SELECT * FROM bares", function(err, bares) {
		res.send(bares);
	});
	
});

app.get('/misbares/bares/:id', function(req, res) { 
	console.log('GET /misbares/bares/' + req.params.id); 
	db.all("SELECT * FROM bares WHERE id = ?", req.params.id, function(err, bar) {
		res.send(bar);
	});
	
	res.status(404).send('Not found');
});

app.put('/misbares/bares/:id', function(req, res) { 
	console.log('PUT /misbares/bares/' + req.params.id); 
	var bar = req.body;
	db.run("UPDATE bares SET titulo = ? , visible = ? , descripcion = ? , fecha = ? , posiciones = ?, fotos = ?, direccion = ?, latitud = ?, longitud = ? WHERE id = ?", [bar.titulo, bar.visible, bar.descripcion, bar.fecha, JSON.stringify(bar.posiciones), JSON.stringify(bar.fotos), bar.direccion, bar.latitud, bar.longitud, req.params.id], function(err) { 
		if (err) { 
			console.log('Error: ' + err); 
			res.status(404).send('Not found'); 
			return;
		}
		else {
			//console.log(this.lastID);
			return;
		}	
	});
	res.send(bar);
	
});

app.delete('/misbares/bares/:id', function(req, res) {
	console.log('DELETE /misbares/bares/' + req.params.id); 
	db.run("DELETE FROM bares WHERE id = ?", [req.params.id], function(err) { 
		if (err) { 
			console.log('Error: ' + err); 
			res.status(404).send('Not found'); 
			return;
		}
		else  { 
			res.status(200).send();
			return;
		}	
	});
    
});
    
app.listen(8080);

console.log('Servidor arrancado!');
