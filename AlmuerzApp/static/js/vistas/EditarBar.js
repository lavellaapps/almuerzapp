var EditarBar = Backbone.View.extend({
	initialize: function() {
		this.$('#btBorrar').prop("disabled", true);
	},
	render : function() {
		this.$('#txtEditarTitulo').val(this.model.get('titulo')).textinput('refresh');
		this.$('#txtEditarFecha').val(this.model.get('fecha')).textinput('refresh');
		this.$('#txtEditarDescripcion').val(this.model.get('descripcion')).textinput('refresh');
		this.$('#txtEditarDireccion').val(this.model.get('direccion')).textinput('refresh');
		this.$('#txtEditarPosicionLat').val(this.model.get('latitud')).textinput('refresh');
		this.$('#txtEditarPosicionLon').val(this.model.get('longitud')).textinput('refresh');
		this.$('#txtEditarVisualizar').val(this.model.get('visible')).flipswitch('refresh');

		this.$('#txtEditarModificar').val('off').flipswitch('refresh');
		this.$('#btBorrar').button('refresh');
	},
	events : {
		'change #txtEditarModificar' : function() {
			if(this.$('#txtEditarModificar').val() == "on"){
				this.$('#txtEditarTitulo').prop("readonly", false);
				this.$('#txtEditarDescripcion').prop("readonly", false);
				this.$('#txtEditarDireccion').prop("readonly", false);
				this.$('#txtEditarPosicionLat').prop("readonly", false);
				this.$('#txtEditarPosicionLon').prop("readonly", false);
				this.$('#txtEditarVisualizar').prop("readonly", false);
				this.$('#btBorrar').prop("disabled", false).button('refresh');;
			}
			else{
				this.$('#txtEditarTitulo').prop("readonly", true);
				this.$('#txtEditarDescripcion').prop("readonly", true);
				this.$('#txtEditarDireccion').prop("readonly", true);
				this.$('#txtEditarPosicionLat').prop("readonly", true);
				this.$('#txtEditarPosicionLon').prop("readonly", true);
				this.$('#btBorrar').prop("disabled", true).button('refresh');;
			}
		},
		'change #txtEditarTitulo' : function() 
		{
			this.model.set('titulo', this.$('#txtEditarTitulo').val());
		},
		'change #txtEditarDescripcion' : function() 
		{
			this.model.set('descripcion', this.$('#txtEditarDescripcion').val());
		},
		'change #txtEditarDireccion' : function() 
		{
			this.model.set('direccion', this.$('#txtEditarDireccion').val());
		},
		'change #txtEditarPosicionLat' : function() 
		{
			this.model.set('latitud', this.$('#txtEditarPosicionLat').val());
		},
		'change #txtEditarPosicionLon' : function() 
		{
			this.model.set('longitud', this.$('#txtEditarPosicionLon').val());
		},
		'change #txtEditarVisualizar' : function() {
			this.model.set('visible', this.$('#txtEditarVisualizar').val());
		},
		'click #btBorrar' : function() {
			this.collection.remove(this.model);
			$(':mobile-pagecontainer').pagecontainer('change', '#pgListado');
		}
	}
});