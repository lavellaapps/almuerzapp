var VerBar = Backbone.View.extend({
	render : function() {
		this.$('#txtVerTitulo').val(this.model.get('titulo')).textinput('refresh');
		this.$('#txtVerFecha').val(this.model.get('fecha')).textinput('refresh');
		this.$('#txtVerDescripcion').val(this.model.get('descripcion')).textinput('refresh');
		this.$('#txtVerDireccion').val(this.model.get('direccion')).textinput('refresh');
		this.$('#txtVerPosicionLat').val(this.model.get('latitud')).textinput('refresh');
		this.$('#txtVerPosicionLon').val(this.model.get('longitud')).textinput('refresh');
		this.$('#txtVerVisualizar').val(this.model.get('visible')).flipswitch('refresh');
		//this.$('#txtVerVisualizar').val(this.model.get('visible')).textinput('refresh');
	},
	events : {
		'click #btEditar' : function() {
			// prueba para llamar a otra ventana para edicion

			// this.vistaEditarBar = new EditarBar({collection: this.collection, el: '#pgEditarBar'});
			//console.log(this.model.get('id'));
			//this.vistaEditarBar.model = this.collection.get(this.model.get('id'));
			//$(':mobile-pagecontainer').pagecontainer('change', '#pgEditarBar');
			//this.vistaEditarBar.render();
			
			// prueba para cambiar la misma ventana a modo edicion
			this.$('#txtVerTitulo').prop("readonly", false);
			console.log(this.$('#txtVerTitulo').prop("readonly"));
	
		},
		'change #txtEditarTitulo' : function() 
		{
			this.model.set('titulo', this.$('#txtEditarTitulo').val());
		},
		'change #txtEditarDescripcion' : function() 
		{
			this.model.set('descripcion', this.$('#txtEditarDescripcion').val());
		},
		'change #txtEditarDireccion' : function() 
		{
			this.model.set('direccion', this.$('#txtEditarDireccion').val());
		},
		'change #txtEditarPosicionLat' : function() 
		{
			this.model.set('latitud', this.$('#txtEditarPosicionLat').val());
		},
		'change #txtEditarPosicionLon' : function() 
		{
			this.model.set('longitud', this.$('#txtEditarPosicionLon').val());
		},
		'change #txtEditarVisualizar' : function() {
			this.model.set('visible', this.$('#txtEditarVisualizar').val());
			console.log(this.$('#txtEditarVisualizar').val());
			console.log(this.model.get('titulo'));
			this.$('#txtEditarDescripcion').readOnly = true;
		},
	},
	
	editarBar : function(vistaEditarBar) {
		this.vistaEditarBar = vistaEditarBar;
		//console.log(this.vistaEditarBar.collection.get(1));
	}
	/*
	,
	abrirEditarBar : function(e) {
		// recuperar id
		var id = $(e.target).attr('id');
		// console.log('abrirEditarBar (' + id + ')');
		this.vistaEditarBar.model = this.collection.get(id);
		$(':mobile-pagecontainer').pagecontainer('change', '#pgEditarBar');
		this.vistaEditarBar.render();
	}
	*/
});