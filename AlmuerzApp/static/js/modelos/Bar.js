var Bar = Backbone.Model.extend({
	urlRoot : 'http://localhost:8080/misbares/bares',
	//urlRoot : 'http://192.168.0.102:8080/misbares/bares',
	initialize : function() {
		if (!this.has("posiciones"))
			this.set('posiciones', []);
		else {
			this.set('posiciones', JSON.parse(this.get('posiciones')));
		}
		if (!this.has("fotos"))
			this.set('fotos', []);
		else {
			this.set('fotos', JSON.parse(this.get('fotos')));
		}
		if (!this.has("fecha"))
			this.set('fecha', Date());
	},
	defaults : {
		titulo : 'Nuevo Bar',
		fecha : Date(),
		visible : 'on',
		latitud : 0.0,
		longitud : 0.0
	},
});
