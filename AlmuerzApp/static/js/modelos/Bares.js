/**
 * Define una coleccion de bares
 */

function isNumber(n) {
	return !isNaN(parseInt(n)) && isFinite(n);
}

var Bares = Backbone.Collection.extend({
	url : 'http://localhost:8080/misbares/bares',
	//url : 'http://192.168.0.102:8080/misbares/bares',
	model : Bar,
	initialize : function() {
		this.on("add", function(model, col, opt) {
			//console.log('Bares:add ' + model.id);			
			model.save();
			console.log('Bares:add ' + model.id);	
		});
		this.on("remove", function(model, col, opt) {
			console.log('Bares:remove ' + model.id);
			model.destroy({silent: true});
		});
		this.on("change", function(model, opt) {
			console.log('Bares:change ' + model.id);
			if (model.changedAttributes().id) {
				return;
			}
			model.save();
		});
		this.fetch({ reset: true });
	},
	import : function() {
		console.log('Bares:import');
		for (var i = 0; i < localStorage.length; i++) {
			var key = localStorage.key(i);
			if (isNumber(key)) {
				var val = localStorage.getItem(key);
				var r = JSON.parse(val);
				this.push(r);
			}
		}
	},
	filtrarPorNombre: function (nombre) {
		if(nombre == "") 
			return this;

	    var pattern = new RegExp(nombre,"gi");	    
	    
        filtered = this.filter(function (bar) {
            return pattern.test(bar.get("titulo"));
        });
        
        return new Bares(filtered);
    }
});

