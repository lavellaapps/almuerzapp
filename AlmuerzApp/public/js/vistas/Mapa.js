var Mapa = Backbone.View.extend({
	initialize : function() {
		var self = this;
		// crear el mapa la primera vez que se muestra la pagina
		$(document).one(
				'pageshow',
				'#' + this.$el.attr('id'),
				function(e, data) {
					// crear contenedor
					self.$('.ui-content').append('<div id="pnMapa"></div>');
					// obtener altura del contenedor
					var header = $.mobile.activePage
							.find("div[data-role='header']:visible");
					//console.log(header.outerHeight());
					var search = $.mobile.activePage
							.find("div.ui-content:visible");
					//console.log(search.outerHeight());
					var footer = $.mobile.activePage
							.find("div[data-role='footer']:visible");
					var content = $.mobile.activePage
							.find("div.ui-content:visible");
					var viewport_height = $(window).height();
					var content_height = viewport_height - search.outerHeight()
							- footer.outerHeight();
					if ((content.outerHeight() - search.outerHeight() - footer
							.outerHeight()) <= viewport_height) {
						content_height -= (content.outerHeight() - content
								.height());
					}
					//self.$('#pnMapa').height(content_height - 40); //40 de los margenes ? 
					self.$('#pnMapa').height(content_height); 
					// crear mapa
					var myOptions = {
						zoom : 18,
						center : new google.maps.LatLng(39.46981860, -0.32571000),
						mapTypeId : google.maps.MapTypeId.ROADMAP
					};
					self.map = new google.maps.Map(self.$('#pnMapa')[0], myOptions);
					self.markers = new Array();
					
					// geolocalizar el buscador
					self.$("#searchMapa").geocomplete({
						  map: self.map
					});
					
					// pintar
					self.render();					
					
				});
		
		// controlar cambios en el modelo
		this.collection.on('remove', function() {
			self.render();
		});
		this.collection.on('sync', function() {
			self.render();
		});
	},
	render : function() {
		
		// limpiar todas los markers pintados
		for (var i = 0; i < this.markers.length; i++)
			this.markers[i].setMap(null);
		this.markers = [];
		
		// pintar los bares
		var self = this;	
		
		// aplicar filtros
		var bares_filtrados = this.collection;
		
		// nombre
		//bares_filtrados = bares_filtrados.filtrarPorNombre('La Pascuala');
		// tipo, distancia, valoracion...
		
		bares_filtrados.forEach(function(bar) {
			// solo si son visibles
			if (bar.get('visible') == 'on') {
				var image = {
					    url: 'images/marker.png', // url
					    scaledSize: new google.maps.Size(50, 50), // scaled size
					    origin: new google.maps.Point(0, 0), // origin
					    anchor: new google.maps.Point(20, 20) // anchor
					};
				var marker = new google.maps.Marker({
					position : new google.maps.LatLng(bar.get('latitud'), bar.get('longitud')),
					map : self.map,
					title : bar.get('titulo'),
					icon: image
				});
				
				google.maps.event.addListener(marker, 'click', function() {

					//self.$('#poppphoto').attr("src", foto.uri);
					self.$('#poppphoto').attr("src", "images/chivito.jpeg");	
					self.$('#poptitulo').text(bar.get('titulo'));
					self.$('#popdescripcion').text(bar.get('descripcion'));
					self.$('#popmas').text(bar.get('id'));
					self.$('#popupPhoto').popup("open", {
						transition : 'fade'
					});
				});		

				// guardar informacion sobre los markers
				self.markers.push(marker);
			}
		});		
	},
	verBar : function(vistaVerBar) {
		this.vistaVerBar = vistaVerBar;
	},
	events : {
		'click #popmas' : function() {
			/*
			var id = self.$('#popmas').text();
			this.vistaVerBar.model = this.collection.get(id);
			$(':mobile-pagecontainer').pagecontainer('change', '#pgVerBar');
			this.vistaVerBar.render();
			*/
		} ,
		'click #btnSearch' : function() {
			this.$('#popupSearch').popup("open", {
				transition : 'fade'
			});
		}
	}
});